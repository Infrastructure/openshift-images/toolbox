FROM registry.access.redhat.com/ubi9/ubi:latest

RUN dnf install -y wget && \
    dnf clean all

RUN wget https://mirror.openshift.com/pub/openshift-v4/x86_64/clients/ocp/stable-4.10/openshift-client-linux.tar.gz && \
    tar -xvf openshift-client-linux.tar.gz && \
    mv oc kubectl /usr/local/bin/ && \
    rm -rf openshift-client-linux.tar.gz
